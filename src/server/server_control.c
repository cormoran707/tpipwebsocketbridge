/*
  server_contorl.c

  制御通信操作用プロトコルの実装

  受け入れる データ

  {
      digital : array or { 1 : hoge }, 
	  pwm     :                      ,
	  motor   :                      ,
      
      get_control : bool, これを送ると現在の出力状況を格納したjsonを送り返す
      streaming   : bool, センサー値のストリーミングをするかどうか設定する（初期値ON）
    
      その他、カスタムで設定したコマンドを入れられる
  }

  送り出す データ
  
  {
      digital : array(bool)
      analog  : array
      pulse   : array
      battery : real , 電圧(V)
      status  : string , 現状 "Connected" | "Not Connected"
 
      その他、カスタムで設定したコマンドを入れられる
  }

 */

#include "server_control.h"
#include "private.h"

#include "jansson.h"
#include "tpip.h"
#include "tpip_json.h"

#include "robot.h"

#include "mytime.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

//
//
// private declearation
//
//

// struct

struct per_session_data__control {
    int streaming_duration_ms;     // 送る周期の最大値
    mytime_t last_streamed_time;
    bool request_sensor_streaming; // センサー値をストリーミングするかどうか
    bool request_current_control_output;   // 出力設定状態を送る時用のflg
    bool request_server_config;          // サーバー設定要求flg
};


// function

static int callback_control(struct lws *wsi,
                            enum lws_callback_reasons reason,
                            void *user,
                            void *in,
                            size_t len);

static bool update_user_setting(json_t *root,
                                       struct lws *wsi,
                                       struct per_session_data__control *user);

static void update_control_data_buffer();
static void update_sensor_data_buffer();
static void update_server_config_data_buffer();

// variable
    
static struct {
    struct lws_context *context;
    int user_num;
    lws_send_buffer send_control_buf;
    lws_send_buffer send_sensor_buf;
    lws_send_buffer send_server_config_buf;
} local_;


//
//
// public
//
//

struct lws_protocols server_protcol_control = {
    "control",
    callback_control,
    sizeof(struct per_session_data__control),
    400
};


bool server_init_control(struct lws_context *context) {
    if(context == NULL) return false;
    local_.context = context;
    local_.user_num = 0;
    update_sensor_data_buffer();
    update_control_data_buffer();
    return true;
}

bool server_tasks_control() {
    if(local_.context == NULL) return false;
    update_sensor_data_buffer();
    lws_callback_on_writable_all_protocol(local_.context,
                                          &server_protcol_control);
    return true;
}

extern bool server_destroy_control() {
    local_.context = NULL;
    return true;
}

//
//
// private
//
//

static int on_connection(struct lws *wsi, struct per_session_data__control *user_data) {
    local_.user_num += 1;
    user_data->request_sensor_streaming = true;
    user_data->request_current_control_output = false;
    user_data->request_server_config = false;
    user_data->last_streamed_time = mytime_get_time();
    user_data->streaming_duration_ms = 100;	
    return 0;
}


static int on_writable(struct lws *wsi,
                       struct per_session_data__control *user_data) {
    if(user_data->request_current_control_output) {
        lws_send_buffer_send(&local_.send_control_buf,
                             wsi, LWS_WRITE_TEXT);
        user_data->request_current_control_output = false;
    }
	if (user_data->request_server_config) {
		lws_send_buffer_send(&local_.send_server_config_buf, wsi, LWS_WRITE_TEXT);
		user_data->request_server_config = false;
	}
    if(user_data->request_sensor_streaming) {
        time_t now = mytime_get_time();
        time_t prev = user_data->last_streamed_time;
        int duration = user_data->streaming_duration_ms;
        
        if((double)(now - prev) / CLOCKS_PER_SEC * 1000 >= duration) {
            lws_send_buffer_send(&local_.send_sensor_buf,
                                 wsi, LWS_WRITE_TEXT);
            user_data->last_streamed_time = now;
        }
    }
    return 0;
}


static int on_receive(struct lws *wsi,
                      struct per_session_data__control *user_data,
                      const char* in_str) {
    json_error_t err;
    json_t *root = json_loads(in_str, 0, &err);
    if(!root) {
        fprintf(stderr, "%s error: on line %d: %s\n",
                __func__, err.line, err.text);
        return -1; // reject & close connection
    }
    
    // tpip_set_control_by_json(root); // @note custom で呼ぶように変更
    update_user_setting(root, wsi, user_data);
    robot_on_receive(root);
    
    json_decref(root);
    return 0;
}


static int on_close(struct lws *wsi,
                    struct per_session_data__control *user_data) {
    local_.user_num -= 1;
    return 0;
}


//
//
// 補助関数
//
//

static void update_server_config_data_buffer() {
	json_t *data = json_object();
	json_object_set_new(data, "target", json_string(tpip_get_target_ip()));
	char *data_str = json_dumps(data, JSON_COMPACT);
	lws_send_buffer_set(&local_.send_server_config_buf, data_str, strlen(data_str));
	free(data_str);
	json_decref(data);
}

static void update_control_data_buffer() {
    json_t *data = tpip_get_control_as_json();
    char *data_str = json_dumps(data, JSON_COMPACT);
    lws_send_buffer_set(&local_.send_control_buf, data_str, strlen(data_str));
    free(data_str);
    json_decref(data);
}

static void update_sensor_data_buffer() {
    json_t *data = json_object();
    // @note センサー値はカスタム側で追加する
	// new
	json_t *connection = json_object();
    json_object_set_new(connection, "tpip", json_boolean(tpip_is_control_connected()));
    json_object_set_new(connection, "control", json_boolean(tpip_is_controlboard_working()));
	json_object_set_new(data, "connection", connection);

    robot_on_send(data); // カスタムのセンサー値を追加など
    
    char *data_str = json_dumps(data, JSON_COMPACT);
    lws_send_buffer_set(&local_.send_sensor_buf, data_str, strlen(data_str));
    free(data_str);
    json_decref(data);
}

static bool update_user_setting(json_t *root, struct lws *wsi,
                                    struct per_session_data__control *user) {

    if(!json_is_object(root)) return false;

    bool request_control_output = false;
	bool request_server_config = false;
    {
        json_t *value = json_object_get(root, "get_control");
        if(json_is_boolean(value)) {
            user->request_current_control_output = true;
            request_control_output = true;
        }
    }
	{
        json_t *value = json_object_get(root, "get_config");
        if(json_is_boolean(value)) {
            user->request_server_config = true;
            request_server_config = true;
        }
	}
    {
        json_t *value = json_object_get(root, "streaming");
        if(json_is_boolean(value)) {
            user->request_sensor_streaming = json_boolean_value(value);
        }
    }
    {
        json_t *value = json_object_get(root, "fps");
        if(json_is_integer(value)) {
            json_int_t fps = json_integer_value(value);
            user->streaming_duration_ms = 1.0 / fps * 1000;
        }
    }
    if(request_control_output) {
        update_control_data_buffer();
        lws_callback_on_writable(wsi);
    }
    if (request_server_config) {
        update_server_config_data_buffer();
        lws_callback_on_writable(wsi);
    }
    return true;
}



int callback_control(struct lws *wsi, enum lws_callback_reasons reason,
                     void *user, void *in, size_t len) {
    switch (reason) {
        case LWS_CALLBACK_ESTABLISHED:
            lwsl_notice("Connected client to control server.\n");
            on_connection(wsi, (struct per_session_data__control*)user);
            break;
        case LWS_CALLBACK_CLOSED:
            lwsl_notice("Disconnected client from control server.\n");
            on_close(wsi, (struct per_session_data__control*)user);
            break;
        case LWS_CALLBACK_SERVER_WRITEABLE:
            on_writable(wsi, (struct per_session_data__control*)user);
            break;
        case LWS_CALLBACK_RECEIVE:
            on_receive(wsi, (struct per_session_data__control*)user, (const char*)in);
            break;
        case LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION:
            dump_handshake_info(wsi);
            break;
    }
    return 0;
}
