#ifndef SERVER_CONTROL_H
#define SERVER_CONTROL_H

#include "libwebsockets.h"
#include <stdbool.h>

//
// 制御サーバー
//
#ifdef __cplusplus
extern "C" {
#endif

extern struct lws_protocols server_protcol_control;


extern bool server_init_control(struct lws_context *context);
extern bool server_tasks_control();
extern bool server_destroy_control();

#ifdef __cplusplus
}
#endif

#endif