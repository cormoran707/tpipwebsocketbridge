#ifndef SERVER_CAMERA_H
#define SERVER_CAMERA_H

#include <stdbool.h>

#include "libwebsockets.h"

//
// カメラサーバー
//

#ifdef __cplusplus
extern "C" {
#endif

extern struct lws_protocols server_protcol_camera;


extern bool server_init_camera(struct lws_context *context);
// これを呼ぶ周期が最大フレームレートになる
extern bool server_tasks_camera();
extern bool server_destroy_camera();

#ifdef __cplusplus
}
#endif

#endif