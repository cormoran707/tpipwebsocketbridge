﻿
#include "server_http.h"
#include "private.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

//
//
// private declearation
//
//

// struct

struct per_session_data__http {
    lws_filefd_type fd;
    unsigned int client_finished : 1;
};

// function

extern int callback_http(struct lws *wsi,
                         enum lws_callback_reasons reason,
                         void *user,
                         void *in,
                         size_t len);


// variable

static struct {
    struct lws_context *context;
    const char *resource_path;
} local_;

//
//
// public
//
//

struct lws_protocols server_protcol_http = {
    "http",
    callback_http,
    sizeof (struct per_session_data__http),
    0,
};

bool server_init_http(struct lws_context *context, const char *resource_path) {
    if(context == NULL) return false;
    local_.context = context;
    local_.resource_path = resource_path;
    return true;
}

bool server_destroy_http() {
    local_.context = NULL;
    return true;
}

//
//
// private
//
//

static const char * get_mimetype(const char *file){
    int n = strlen(file);
    if (n < 5)
        return NULL;
    
    if (!strcmp(&file[n - 4], ".ico"))
        return "image/x-icon";
    
    if (!strcmp(&file[n - 4], ".png"))
        return "image/png";
    
    if (!strcmp(&file[n - 5], ".html"))
        return "text/html";
    
    if (!strcmp(&file[n - 3], ".js"))
        return "text/javascript";
    
    if (!strcmp(&file[n - 4], ".css"))
        return "text/css";
	if (!strcmp(&file[n - 4], ".ttf"))
		return "application/font-ttf";
	if (!strcmp(&file[n - 5], ".woff"))
		return "application/font-woff";
	if (!strcmp(&file[n - 4], ".svg"))
		return "image/svg+xml";
	if (!strcmp(&file[n - 6], ".woff2"))
		return "application/font-woff2";

    return NULL;
}

/*
 * We take a strict whitelist approach to stop ../ attacks
 */
struct serveable {
    const char *urlpath;
    const char *mimetype;
};



int callback_http(struct lws *wsi,
                  enum lws_callback_reasons reason,
                  void *user, void *in, size_t len) {
    struct per_session_data__http *pss = (struct per_session_data__http *)user;
    unsigned char buffer[4096 + LWS_PRE];
    unsigned long amount, sent;
    const char *mimetype;
    char *other_headers;
    char buf[256];
    int n, m;
    
    const char *in_str = (const char*)in;
    
    switch (reason) {
        case LWS_CALLBACK_HTTP:
        {
            char name[100], rip[50];
            lws_get_peer_addresses(wsi, lws_get_socket_fd(wsi), name,
                                   sizeof(name), rip, sizeof(rip));
            sprintf(buf, "%s (%s)", name, rip);
            lwsl_notice("HTTP connect from %s\n", buf);
        }
            
            if (len < 1) {
                lws_return_http_status(wsi, HTTP_STATUS_BAD_REQUEST, NULL);
                goto try_to_reuse;
            }
            
            // アクセスが階層構造だったら拒否する（ことにする）
            //if (strchr(in_str + 1, '/')) {
            //    lws_return_http_status(wsi, HTTP_STATUS_FORBIDDEN, NULL);
            //    goto try_to_reuse;
            //}
            
            // POSTの時処理
            if (lws_hdr_total_length(wsi, WSI_TOKEN_POST_URI)) {
                // POST
                printf("%s", in_str);
                return 0;
            }
            
            // 要求パスを正規化して取り出す
            // {resource_path}/...な感じ
            strcpy(buf, local_.resource_path);
            if (strcmp(in_str, "/")) {
                if (*in_str != '/') strcat(buf, "/");
                strncat(buf, in_str, sizeof(buf) - strlen(buf) - 1);
            } else {
                strcat(buf, "/index.html");
            }
            buf[sizeof(buf) - 1] = '\0';
            
			if (buf[strlen(buf) - 1] == '/') {
				strncat(buf, "index.html", sizeof(buf) - strlen(buf) - 1);
				buf[sizeof(buf) - 1] = '\0';
			}


            // mimetypeのチェック
            mimetype = get_mimetype(buf);
            if (!mimetype) {
                lwsl_err("Unknown mimetype for %s\n", buf);
                lws_return_http_status(wsi, HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE, NULL);
                return -1;
            }
            n = 0;
            n = lws_serve_http_file(wsi, buf, mimetype, other_headers, n);
            if (n < 0 || ((n > 0) && lws_http_transaction_completed(wsi)))
                return -1; /* error or can't reuse connection: close the socket */
            
            // 送信が非同期で行われる
            // 完了時は　LWS_CALLBACK_HTTP_FILE_COMPLETION
            break;
            
        case LWS_CALLBACK_HTTP_BODY:
            strncpy(buf, (const char*)in, 20);
            buf[20] = '\0';
            if (len < 20)
                buf[len] = '\0';
            
            lwsl_notice("LWS_CALLBACK_HTTP_BODY: %s... len %d\n",
                        (const char *)buf, (int)len);
            
            break;
            
        case LWS_CALLBACK_HTTP_BODY_COMPLETION:
            lwsl_notice("LWS_CALLBACK_HTTP_BODY_COMPLETION\n");
            /* the whole of the sent body arrived, close or reuse the connection */
            lws_return_http_status(wsi, HTTP_STATUS_OK, NULL);
            goto try_to_reuse;
            
        case LWS_CALLBACK_HTTP_FILE_COMPLETION:
            goto try_to_reuse;
            
        case LWS_CALLBACK_HTTP_WRITEABLE:
            lwsl_info("LWS_CALLBACK_HTTP_WRITEABLE\n");
            
            if (pss->client_finished)
                return -1;
            
            if (pss->fd == LWS_INVALID_FILE)
                goto try_to_reuse;
            
            /*
             * we can send more of whatever it is we were sending
             */
            sent = 0;
            do {
                /* we'd like the send this much */
                n = sizeof(buffer) - LWS_PRE;
                
                /* but if the peer told us he wants less, we can adapt */
                m = lws_get_peer_write_allowance(wsi);
                
                /* -1 means not using a protocol that has this info */
                if (m == 0)
                /* right now, peer can't handle anything */
                    goto later;
                
                if (m != -1 && m < n)
                /* he couldn't handle that much */
                    n = m;
                
                n = lws_plat_file_read(wsi, pss->fd,
                                       &amount, buffer + LWS_PRE, n);
                /* problem reading, close conn */
                if (n < 0) {
                    lwsl_err("problem reading file\n");
                    goto bail;
                }
                n = (int)amount;
                /* sent it all, close conn */
                if (n == 0)
                    goto penultimate;
                /*
                 * To support HTTP2, must take care about preamble space
                 *
                 * identification of when we send the last payload frame
                 * is handled by the library itself if you sent a
                 * content-length header
                 */
                m = lws_write(wsi, buffer + LWS_PRE, n, LWS_WRITE_HTTP);
                if (m < 0) {
                    lwsl_err("write failed\n");
                    /* write failed, close conn */
                    goto bail;
                }
                if (m) /* while still active, extend timeout */
                    lws_set_timeout(wsi, PENDING_TIMEOUT_HTTP_CONTENT, 5);
                sent += m;
                
            } while (!lws_send_pipe_choked(wsi) && (sent < 1024 * 1024));
        later:
            lws_callback_on_writable(wsi);
            break;
        penultimate:
            lws_plat_file_close(wsi, pss->fd);
            pss->fd = LWS_INVALID_FILE;
            goto try_to_reuse;
            
        bail:
            lws_plat_file_close(wsi, pss->fd);
            
            return -1;
            
            /*
             * callback for confirming to continue with client IP appear in
             * protocol 0 callback since no websocket protocol has been agreed
             * yet.  You can just ignore this if you won't filter on client IP
             * since the default unhandled callback return is 0 meaning let the
             * connection continue.
             */
        case LWS_CALLBACK_FILTER_NETWORK_CONNECTION:
        case LWS_CALLBACK_GET_THREAD_ID:
        default:
            break;
    }
    
    return 0;
    
    /* if we're on HTTP1.1 or 2.0, will keep the idle connection alive */
try_to_reuse:
    if (lws_http_transaction_completed(wsi))
        return -1;
    
    return 0;
}
