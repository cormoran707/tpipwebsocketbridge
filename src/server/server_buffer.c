#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "private.h"

void lws_send_buffer_set(lws_send_buffer *buffer, void* src, size_t len) {
    if(buffer->allocated == 0) {
        buffer->allocated = 1;
        buffer->buffer = (uint8_t*)malloc(1);
    }
    while(buffer->allocated < LWS_SEND_BUFFER_PRE_PADDING + len) buffer->allocated *= 2;
    buffer->buffer = (uint8_t*)realloc(buffer->buffer, buffer->allocated);
    assert(buffer->buffer != NULL);
    memcpy(buffer->buffer + LWS_SEND_BUFFER_PRE_PADDING, src, len);
    buffer -> size = len;    
}

void lws_send_buffer_send(lws_send_buffer *buffer, struct lws *wsi, enum lws_write_protocol protcol) {
    lws_write(wsi, buffer->buffer + LWS_SEND_BUFFER_PRE_PADDING,
              buffer->size ,protcol );    
}

void lws_send_buffer_free(lws_send_buffer *buffer) {
    free(buffer->buffer);
    buffer->buffer = 0;
    buffer->size = 0;
    buffer->allocated = 0;    
}