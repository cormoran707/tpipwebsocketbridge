#ifndef PRIVATE_H
#define PRIVATE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "libwebsockets.h"

#ifdef __cplusplus
extern "C" {
#endif
    
//
// send buffer
//
    
typedef struct lws_send_buffer {
    uint8_t *buffer;    
    size_t allocated;
    size_t size;
} lws_send_buffer;

extern void lws_send_buffer_set(lws_send_buffer *buffer, void* src, size_t len);

extern void lws_send_buffer_send(lws_send_buffer *buffer, struct lws *wsi,enum lws_write_protocol protcol);

extern void lws_send_buffer_free(lws_send_buffer *buffer);

//
// utils
//
    
extern void dump_handshake_info(struct lws *wsi);
extern const char* lws_get_reason_string(int reason);
    
#ifdef __cplusplus
}
#endif

#endif