#include "mytime.h"
#include "tpip_config.h"

#include <time.h>
#ifdef TPLX2
#include <sys/time.h>
#endif

mytime_t mytime_get_time() {
#ifdef TPLX2
    struct timeval s;
    gettimeofday(&s, NULL);
    return (s.tv_sec % 1000 * 1000000 + s.tv_usec);
#else
    return (mytime_t) clock();
#endif
}

