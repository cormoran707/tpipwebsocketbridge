#include <string.h>
#include "tpip_json.h"


//
//
// JSON インターフェースの関数
//
//

json_t* tpip_get_sensor_as_json() {
    struct tpip_sensor_data sens;
    tpip_get_sensor(&sens);
    return tpip_convert_sensor_to_json(&sens);
}

json_t *tpip_get_control_as_json() {
    struct tpip_control_data ctl;
    tpip_get_control(&ctl);
    return tpip_convert_control_to_json(&ctl);
}

bool tpip_set_control_by_json(const json_t* json_data) {
    struct tpip_control_data ctl;
    tpip_get_control(&ctl);
    if(tpip_convert_json_to_control(json_data, &ctl) ){
        tpip_set_control(&ctl);
        return true;
    } else {
        return false;
    }
}


//
//
// JSON <=> 構造体 変換関数
//
//


json_t* tpip_convert_sensor_to_json(const struct tpip_sensor_data *sens) {
    json_t *root = json_object();
    {
        json_t *digital = json_array();
        for(int i = 0; i < TPIP_DIGITAL_IN_COUNT; i++){
            json_array_append_new(digital, json_boolean(sens->digital[i]));
        }
        json_object_set_new(root, "digital", digital);
    }
    {
        json_t *analog = json_array();
        for(int i = 0; i < TPIP_ANALOG_IN_COUNT; i++){
            json_array_append_new(analog, json_real((double)sens->analog[i] / TPIP_ANALOG_IN_MAX * 100.0));
        }
        json_object_set_new(root, "analog", analog);
    }
    {
        json_t *pulse = json_array();
        for(int i = 0; i < TPIP_PULSE_IN_COUNT; i++){
            json_array_append_new(pulse, json_integer(sens->pulse[i]));
        }
        json_object_set_new(root, "pulse", pulse);
    }
    {
        json_object_set_new(root, "battery", json_real(sens->battery / 100.0));
    }
    return root;
}


bool tpip_convert_json_to_control(const json_t *root, struct tpip_control_data *ctl) {
    if(! root || !json_is_object(root)) {
        fprintf(stderr, "%s error: root is not an array\n", __func__);
        return false;
    }
    {
        json_t *digital = json_object_get(root, "digital");
        if(json_is_object(digital)) {
            json_t *jo;
            const char *key;
            json_object_foreach(digital, key, jo){
                int key_index = atoi(key);
                if( (key_index == 0 && strcmp(key, "0") != 0) ||
                   (key_index < 0 || TPIP_DIGITAL_OUT_COUNT <= key_index) ) continue;
                if(json_is_boolean(jo)){
                    ctl->digital[key_index] = json_boolean_value(jo);
                } // else : do nothing
            }
        } else if(json_is_array(digital)){
            json_t *jo;
            int i;
            json_array_foreach(digital, i, jo) {
                if(i >= TPIP_DIGITAL_OUT_COUNT) continue;
                if(json_is_boolean(jo)){
                    ctl->digital[i] = json_boolean_value(jo);
                } // else : do nothing
            }
        } // else : do nothing
    }
    {
        json_t *pwm = json_object_get(root, "pwm");
        if(json_is_object(pwm)) {
            json_t *jo;
            const char *key;
            json_object_foreach(pwm, key, jo) {
                int key_index = atoi(key);
                if( (key_index == 0 && strcmp(key, "0") != 0) ||
                   (key_index < 0 || TPIP_PWM_OUT_COUNT <= key_index) ) continue;
                if(json_is_number(jo)) {
                    ctl->pwm[key_index] = (int)json_number_value(jo);
                } // else : do nothing
            }
        } else if(json_is_array(pwm)) {
            json_t *jo;
            int i;
            json_array_foreach(pwm, i, jo) {
                if(i >= TPIP_PWM_OUT_COUNT) continue;
                if(json_is_integer(jo)){
                    ctl->pwm[i] = (int)json_integer_value(jo);
                } // else : do nothing
            }
        } // else : do nothing
    }
    
    {
        json_t *motor = json_object_get(root, "motor");
        if(json_is_object(motor)) {
            json_t *jo;
            const char *key;
            json_object_foreach(motor, key, jo) {
                int key_index = atoi(key);
                if( (key_index == 0 && strcmp(key, "0") != 0) ||
                   (key_index < 0 || TPIP_MOTOR_COUNT <= key_index) ) continue;
                if(json_is_integer(jo)) {
                    ctl->motor[key_index] = (int)json_integer_value(jo);
                } // else : do nothing
            }
        } else if(json_is_array(motor)) {
            json_t *jo;
            int i;
            json_array_foreach(motor, i, jo) {
                if(i >= TPIP_MOTOR_COUNT) continue;
                if(json_is_integer(jo)){
                    ctl->motor[i] = (int)json_integer_value(jo);
                } // else : do nothing
            }
        } // else : do nothing
    }
    return true;
}


json_t *tpip_convert_control_to_json(const struct tpip_control_data *ctl) {
    json_t *root = json_object();
    {
        json_t *digital = json_array();
        for(int i = 0; i < TPIP_DIGITAL_OUT_COUNT; i++){
            json_array_append_new(digital, json_boolean(ctl->digital[i]));
        }
        json_object_set_new(root, "digital", digital);
    }
    {
        json_t *pwm = json_array();
        for(int i = 0; i < TPIP_PWM_OUT_COUNT; i++){
            json_array_append_new(pwm, json_integer(ctl->pwm[i]));
        }
        json_object_set_new(root, "pwm", pwm);
    }
    {
        json_t *motor = json_array();
        for(int i = 0; i < TPIP_MOTOR_COUNT; i++){
            json_array_append_new(motor, json_integer(ctl->motor[i]));
        }
        json_object_set_new(root, "motor", motor);
    }
    return root;
}



