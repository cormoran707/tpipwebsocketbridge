/**
 * file   : interval_timer.c
 * date   : 2016-06-22
 */

#include<time.h>
#include<stdlib.h>
#include "interval_timer.h"
#include "tpip_config.h"
#include <stdio.h>

typedef struct interval_timer_t interval_timer_t;

struct interval_timer_t* interval_timer_new() {
    interval_timer_t *t = (interval_timer_t*)malloc(sizeof(interval_timer_t));
    t->next = NULL;
    return t;
}


void interval_timer_set_interval(struct interval_timer_t *timer, void (*f)(void), unsigned long duration_ms) {
    while(timer->next != NULL) timer = timer->next;
    timer->next = (interval_timer_t*)malloc(sizeof(interval_timer_t));
    timer = timer->next;
    timer->next = NULL;
    timer->prev = mytime_get_time();
    timer->duration_s = duration_ms / 1000.0;
    timer->f = f;
}

void interval_timer_tick(struct interval_timer_t *timer) {    
    mytime_t now = mytime_get_time();
    timer = timer->next;
    while(timer != NULL) {
        if((double)(now - timer->prev) / CLOCKS_PER_SEC >= timer->duration_s) {
            timer->f();
            timer->prev = now;
        }
        timer = timer->next;
    }
}

void interval_timer_destroy(struct interval_timer_t *timer) {
    interval_timer_t *t = timer;
    while(t) {
        interval_timer_t *tt = timer->next;
        free(t);
        t = tt;
    }
}
