# 各ロボット用のカスタマイズ方法

（執筆中と思われる）

## プロジェクトの作成

- 以下のコマンド実行 (以下はprojectという名前のフォルダを作ってやる例なので適宜読み替えて）

~~~

mkdir project
cd project
git init
git submodule add https://bitbucket.org/cormoran707/tpipwebsocketbridge
git submodule update --init --recursive
cp -r tpipwebsocketbridge/template/* .

~~~

- project/CMakeLists.txt を適切に編集（プロジェクト名変更等）

## プログラムの作成

- project/src/ に作った *.c, *.cpp, *.h, *.hpp はすべてプロジェクトのソースコードとして認識される
- project/src/custom.cpp に最低限必要な関数等が書かれているのでそこにプログラムを書いていけば良い
- 必要な関数は project/src/内のどこかで定義されていれば良いので、ファイル構成等は自由

## コンパイル

基本的には[コンパイルのページ](readme.compile.md)と同じ

~~~
mkdir build
cd build
cmake .. -DTARGET=TPIP_EMULATOR
# 以下、各ターゲットごとの例
# cmake .. -DTARGET=TPJT2
# cmake .. -DTARGET=TPJT3
# cmake .. -DTARGET=TPLX2
# cmake .. -DTARGET=TPLX3

#unix
make
./<Project Name>
#window
msbuild
.\Debug\<Project Name>

~~~


