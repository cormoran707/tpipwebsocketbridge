#include <stdio.h>

#include "can.h"
#include "custom_contorl.h"

static bool parse_go(json_t *json, struct tpip_can_data *can_data);
static bool parse_roll(json_t *json, struct tpip_can_data *can_data);
static bool parse_stop(json_t *json, struct tpip_can_data *can_data);
static bool parse_free(json_t *json, struct tpip_can_data *can_data);

//
//
// public
//
//

bool parse_mecanum(json_t *json) {
    struct tpip_can_data can_data;
    can_data.standard_id = can_generateId(CAN_TYPE_COMMAND,
                                          CAN_ID_TPIP,
                                          CAN_ID_WHEEL_MASTER);
    if(parse_stop(json, &can_data) || parse_free(json, &can_data) ||
       parse_roll(json, &can_data) || parse_go(json, &can_data)) {
        return tpip_send_can(&can_data);
    }
    return TPIP_ERROR;
}


//
//
// private
//
//

static bool parse_go(json_t *json, struct tpip_can_data *can_data) {
    json_t *obj = json_object_get(json, "go");
    if(json_is_object(obj)) {
        json_t *dir_json = json_object_get(obj, "dir");
        json_t *speed_json = json_object_get(obj, "speed");

        if(json_is_number(dir_json) && json_is_number(speed_json)) {
            int16_t dir;
            uint8_t speed;
            {
                double dir_d = json_number_value(dir_json);
                double speed_d = json_number_value(speed_json);
                if(dir_d > INT16_MAX || INT16_MIN > dir_d) return false;
                if(speed_d > UINT8_MAX || 0 > speed_d) return false;
                dir = dir_d;
                speed = speed_d;
            }

            dir = ((dir % 360) + 360) % 360;
            if(dir > 180) dir -= 360;

            can_data->size = 4;
            can_data->data[0] = CAN_COMMAND_MECANUM_GO;
            memcpy(&can_data->data[1], &dir, sizeof(dir));
            can_data->data[3] = speed;
            // fprintf(stderr, "GO dir : %d speed : %d\n", dir, speed);
            return true;
        }
    }
    return false;
}

static bool parse_roll(json_t *json, struct tpip_can_data *can_data) {
    json_t *obj = json_object_get(json, "roll");
    if(json_is_object(obj)) {
        json_t *dir_json = json_object_get(obj, "dir");
        json_t *speed_json = json_object_get(obj, "speed");
        if(json_is_number(dir_json) && json_is_number(speed_json)) {
            int8_t dir;
            uint8_t speed;
            {
                double dir_d = json_number_value(dir_json);
                double speed_d = json_number_value(speed_json);
                if(speed_d > UINT8_MAX || 0 > speed_d) return false;
                dir = (dir_d == 0 ? 0 : (dir_d > 0 ? 1 : -1));
                speed = speed_d;
            }

            can_data->size = 3;
            can_data->data[0] = CAN_COMMAND_MECANUM_ROLL;
            memcpy(&can_data->data[1], &dir, sizeof(dir));
            can_data->data[2] = speed;
            fprintf(stderr, "ROLL dir : %d speed : %d\n", dir, speed);
            return true;
        }
    }
    return false;
}

static bool parse_stop(json_t *json, struct tpip_can_data *can_data) {
    json_t *obj = json_object_get(json, "stop");
    if(json_is_true(obj)) {
        can_data->size = 1;
        can_data->data[0] = CAN_COMMAND_MECANUM_STOP;
        return true;
    }
    return false;
}

static bool parse_free(json_t *json, struct tpip_can_data *can_data) {
    json_t *obj = json_object_get(json, "free");
    if(json_is_true(obj)) {
        can_data->size = 1;
        can_data->data[0] = CAN_COMMAND_MECANUM_FREE;
        return true;
    }
    return false;
}

