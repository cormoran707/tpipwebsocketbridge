/**
 * robot.h
 * 
 * 各ロボットに特有の制御を書く
 *
 * 実装が必要な関数
 * int robot_init()
 * int robot_on_receive_control_data(const json_t *rcv_json)
 * int robot_on_send_sensor_data(json_t *send_json)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "jansson.h"
#include "tpip.h"
#include "tpip_json.h"

#include "robot.h"
#include "custom_contorl.h"
#include "can.h"

/**
 *
 * TPIP API 初期化直後に呼ばれる関数
 * 初期位置等の設定をすると良い
 * return : -1でエラー（現状何もしない）
 *           0で正常
 *
 */
int robot_setup() {
    return 0;
}


/**
 *
 * 定期的に呼ばれる関数
 * ただし呼ばれる間隔は他の処理の時間によって変動する
 * return : -1でエラー（現状何もしない）
 *           0で正常
 *
 */
int robot_loop() {
    
    return 0;
}


/**
 *
 * 制御情報を受信した時に呼ばれる関数
 *
 * rcv_json : 受信したjsonデータ 向こうで解放されるので勝手にdecrefしないで。
 *
 * return : -1でエラー（接続を打ち切る）
 *           0で正常
 *
 */
int robot_on_receive(const json_t *rcv_json) {
    tpip_set_control_by_json(rcv_json);
    static clock_t prev = 0;
    clock_t now = clock();
#ifdef TPJT2 // TPJT2の仕様対策（CAN通信バッファを溜めないようにする）
    if ((double)(now - prev) / CLOCKS_PER_SEC < 0.095) return 0;
#endif
    prev = now;
    
    const json_t *root = rcv_json;    
    
    json_t *mecanum = json_object_get(root, "mecanum");
    if(json_is_object(mecanum)) {
        parse_mecanum(mecanum);
    }

    json_t *wd4 = json_object_get(root, "4wd");
    if(json_is_object(wd4)) {
        parse_4wd(wd4);
    }

    return 0;
}

/**
 *
 * センサ情報を情報を送信する前に呼ばれる関数
 *
 * send_json : 送信するjsonデータ 制御ボードのセンサ情報が格納されている状態
 *             !! send_json に追加する値は全て所有権を譲渡しないといけない(_newな関数でaddする)
 * return : -1でエラー（送信しない）
 *           0で正常
 *
 */
int robot_on_send(json_t *send_json) {
    {
        json_t *tpip_sensor_json = tpip_get_sensor_as_json();
        json_object_update(send_json, tpip_sensor_json);
        json_decref(tpip_sensor_json);
    }
    json_t *board_status = json_array();
    // 送信するデータを追加する処理や、送信するデータを消してなかったことにする処理など
    if(tpip_is_can_available() > 0) {
        struct tpip_can_data can_data;
        if(tpip_receive_can(&can_data) == TPIP_OK) {
            switch(canGetMessageTypeFromId(can_data.standard_id)) {
                case CAN_TYPE_HEARBEAT:
                    json_array_append_new(board_status,
                                          json_integer(canGetSrcFromId(can_data.standard_id)));
                    break;
                default:
                    break;
            }
        }
    }
    json_object_set_new(send_json, "board_status", board_status);
    return 0;
}


int main() {
    return server_main();
}
