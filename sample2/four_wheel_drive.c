#include "can.h"
#include "custom_contorl.h"

#define MY_MAX(x, y) ((x) > (y) ? (x) : (y))
#define MY_MIN(x, y) ((x) < (y) ? (x) : (y))

static bool parse_go(json_t *json, struct tpip_can_data *can_data);
static bool parse_stop(json_t *json, struct tpip_can_data *can_data);
static bool parse_free(json_t *json, struct tpip_can_data *can_data);

//
//
// public
//
//

bool parse_4wd(json_t *json) {
    struct tpip_can_data can_data;
    can_data.standard_id = can_generateId(CAN_TYPE_COMMAND,
                                          CAN_ID_TPIP,
                                          CAN_ID_WHEEL_MASTER);
    if(parse_stop(json, &can_data) || parse_free(json, &can_data) ||
       parse_go(json, &can_data)) {
        return tpip_send_can(&can_data);
    }
    return TPIP_ERROR;
}


//
//
// private
//
//

static bool parse_go(json_t *json, struct tpip_can_data *can_data) {
    json_t *obj = json_object_get(json, "go");
    if(json_is_object(obj)) {
        json_t *left_json = json_object_get(obj, "right");
        json_t *right_json = json_object_get(obj, "left");
        if(json_is_number(left_json) && json_is_number(right_json)) {
            int16_t left, right;
            {
                double left_d = json_number_value(left_json);
                double right_d = json_number_value(right_json);
                if(left_d > INT16_MAX || INT16_MIN > left_d) return false;
                if(right_d > INT16_MAX || INT16_MIN > right_d) return false;
                left = left_d;
                right = right_d;
            }

            left = MY_MAX(left, -255);
            left = MY_MIN(left, 255);
            right = MY_MAX(right, -255);
            right = MY_MIN(right, 255);

            can_data->size = 5;
            can_data->data[0] = CAN_COMMAND_4WD_GO;            
            memcpy(&can_data->data[1], &left, sizeof(left));
            memcpy(&can_data->data[3], &right, sizeof(right));
            return true;
        }
    }
    return false;
}

static bool parse_stop(json_t *json, struct tpip_can_data *can_data) {
    json_t *obj = json_object_get(json, "stop");
    if(json_is_true(obj)) {
        can_data->size = 1;
        can_data->data[0] = CAN_COMMAND_4WD_STOP;
        return true;
    }
    return false;
}

static bool parse_free(json_t *json, struct tpip_can_data *can_data) {
    json_t *obj = json_object_get(json, "free");
    if(json_is_true(obj)) {
        can_data->size = 1;
        can_data->data[0] = CAN_COMMAND_4WD_FREE;
        return true;
    }
    return false;
}

