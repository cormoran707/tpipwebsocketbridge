#ifndef CAN_H
#define CAN_H

#define CAN_TYPE_EMERGENCY 0
#define CAN_TYPE_COMMAND   1
#define CAN_TYPE_RESPONSE  2
#define CAN_TYPE_DATA      3
#define CAN_TYPE_HEARBEAT  7

#define CAN_ADDR_BROADCAST 0x0

#define can_generateId(type,src,dest) ( \
    (type << 8) | ((src&0xF) << 4) | (dest&0xF))
#define canGetMessageTypeFromId(x) (x >> 8)
#define canGetSrcFromId(x)  ((x & 0xF0) >> 4)
#define canGetDestFromId(x) (x & 0x0F)

#endif
