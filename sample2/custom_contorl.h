#ifndef CUSTOM_CONTROL_H
#define CUSTOM_CONTROL_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "jansson.h"
#include "tpip.h"

// board id(max 15)
#define CAN_ID_TPIP 1

#define CAN_ID_WHEEL_MASTER 2
#define CAN_ID_WHEEL_SLAVE 3

// mecanum command
#define CAN_COMMAND_MECANUM_GO 1
#define CAN_COMMAND_MECANUM_ROLL 2
#define CAN_COMMAND_MECANUM_STOP 3
#define CAN_COMMAND_MECANUM_FREE 4

/*
  for メカナム
  mecanum : {
      stop : true.
      free : true,
      roll : {
          dir : 正 or 0 or 負,
          speed : 0 to 255
      },
      go : {
          dir : -180 to 180,
          speed : 0 to 255
      }
  }
  go, roll, stop, free どれかのみ、複数含まれる場合は上の表の上から優先
  値が範囲外の場合はreject

  @ret : TPIP_ERROR or TPIP_OK
 */
extern bool parse_mecanum(json_t *json);

// 4wd command
#define CAN_COMMAND_4WD_GO 1
#define CAN_COMMAND_4WD_STOP 2
#define CAN_COMMAND_4WD_FREE 3

/*
  for 4wd
  4wd : {
      stop : true.
      free : true,
      go : {
          left : -255 ~ 255,
          right : -255 ~ 255
      }
  }
  go, roll, stop, free どれかのみ、複数含まれる場合は上の表の上から優先

  @ret : TPIP_ERROR or TPIP_OK
 */
extern bool parse_4wd(json_t *json);

#endif

