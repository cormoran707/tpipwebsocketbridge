/**
 * file   : serial_relayed_can.c
 * date   : 2016-07-08
 */

#include <cstdio>
#include <cstdlib>
#include <queue>
#include "serial_relayed_can.h"
#include <string.h>

namespace {
char buff[1025];
std::queue<tpip_can_data> received_data;
struct tpip_can_data receiving_data;
int receiving_pos;
}



bool serial_relayed_can_send(const tpip_can_data *data) {
    bool ret = TPIP_OK;
	ret &= tpip_send_serial("$$", 2);
	//fprintf(stderr, "$$");
	ret &= tpip_send_serial(&data->standard_id, sizeof(data->standard_id));
	//fprintf(stderr, "id %d\n", data->standard_id);
	ret &= tpip_send_serial(&data->size, sizeof(data->size));
	//fprintf(stderr, "send size %d\n", data->size);
    ret &= tpip_send_serial(data->data, data->size);
	ret &= tpip_send_serial("\0\0", 2);
    return ret;
}

// （tasksでデコードしたデータから）データを取り出す
bool serial_relayed_can_receive(tpip_can_data *data) {
    if(received_data.empty()) return TPIP_ERROR;
    else {
        *data = received_data.front();
        received_data.pop();
        return TPIP_OK;
    }
}

// serial recieve -> can パケットにデコード の処理をする
// 定期的に呼び出すこと
bool serial_relayed_can_tasks() {
    int size = 0;
    if(tpip_receive_serial(buff, sizeof(buff), &size)) {
		buff[size] = 0;
		// printf("receive %d %s\n", size, buff);
        for(int i = 0; i < size; i++) {
            if(receiving_pos < 0) {
                if(buff[i] == '#') receiving_pos = 0; // start receive
            } else {
				if (receiving_pos < 2) {
					memcpy(&receiving_data.standard_id + receiving_pos, &buff[i], 1);
					receiving_pos++;
				} else if(receiving_pos == 3) {
					receiving_data.size = buff[i];
					receiving_pos++;
				} else if(receiving_pos < 3 + receiving_data.size) {
                    memcpy(receiving_data.data + receiving_pos - 3,  &buff[i], 1);
                    receiving_pos++;
                } else {
                    // receive complete
                    received_data.push(receiving_data);
                    receiving_pos = -1;
                }
            }
        }
    }
    return TPIP_OK;
}


// receiveしたデータパケット数を返す
int serial_relayed_can_available() {
    return received_data.size();
}
