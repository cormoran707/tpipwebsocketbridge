#ifndef BUFFER_HPP
#define BUFFER_HPP

#include <map>

#include "tpip.h"
#include "serial_relayed_can.h"
#include "can.h"

/*
 * utils
 */

/*
 * send_can_data(dest, ... body)
 * body の部分の値を順にバッファに詰めて送ってくれる関数
 * 各bodyはその型の大きさだけバッファを消費する
 */
/*
template<class T>
constexpr int send_can_data_rec_check() {
    return sizeof(T);
}
template<class First, class... Rest>
constexpr int send_can_data_rec_check(const First &first, const Rest&... rest) {
    return sizeof(First) + send_can_data_rec_check(rest...);
}
*/
static int send_can_data_rec(uint8_t *buff, const int pos) {
    if(pos > 8) {
        std::cerr << "send_can_data_rec : too many data : size " << pos << std::endl;
        assert(pos <= 8);
    }
    return pos;
}

template<class First, class... Rest>
int send_can_data_rec(uint8_t *buff, const int pos, const First &first, const Rest&... rest) {
    memcpy(buff + pos, (void*)&first, sizeof(first));
    return send_can_data_rec(buff, pos + sizeof(First), rest...);
}
/*
template<class... Data>
bool send_can_data(uint8_t dest, const Data&... body) {
    //static_assert(send_can_data_rec_check(body...) <= 8, "");
    struct tpip_can_data data;
    data.standard_id =
            can_generate_standard_id(CAN_TYPE_COMMAND, (uint8_t)CAN_ID::TPIP, dest);
    data.size = send_can_data_rec(data.data, 0, body...);
    return serial_relayed_can_send(&data);
}
*/

template<uint8_t SELF_CAN_ID = 1>
class SendBuffer {
	std::map<std::pair<uint16_t, uint8_t>, struct tpip_can_data> buff;

public:
	void update(struct tpip_can_data &data) {
		buff[std::make_pair(data.standard_id, data.data[0])] = data;
	}
	
	template<class... Data>
	void update(uint8_t dest, const Data&... body) {
		struct tpip_can_data data;
		data.standard_id =
			can_generate_standard_id(CAN_TYPE_COMMAND, SELF_CAN_ID, dest);
		data.size = send_can_data_rec(data.data, 0, body...);
		update(data);
	}

	void send() {
		//std::cerr << "send size " << buff.size() << std::endl;
		for (auto &p : buff) {
			serial_relayed_can_send(&p.second);
		}
		buff.clear();
	}

private:
	int update_rec(uint8_t *buff, const int pos) {
		if (pos > 8) {
			std::cerr << "send_can_data_rec : too many data : size " << pos << std::endl;
			assert(pos <= 8);
		}
		return pos;
	}

	template<class First, class... Rest>
	int update_rec(uint8_t *buff, const int pos, const First &first, const Rest&... rest) {
		memcpy(buff + pos, (void*)&first, sizeof(first));
		return update_rec(buff, pos + sizeof(First), rest...);
	}	
};

#endif
