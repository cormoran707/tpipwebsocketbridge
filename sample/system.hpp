#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include <stdint.h>
#include <cstring>
#include <cassert>
#include <map>
#include <iostream>

#include "jansson.h"
#include "tpip.h"
#include "can.h"
#include "serial_relayed_can.h"
#include "buffer.hpp"

/*
 * ボード定義
 */

enum class CAN_ID : uint8_t {
    BROAD_CAST = 0,
    TPIP ,
    CAN_BOARD_NUM
};

static_assert((int)CAN_ID::CAN_BOARD_NUM <= 15, "CAN BOARD ID should be less than 15");

#endif
