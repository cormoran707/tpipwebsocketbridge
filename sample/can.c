/**
 * file   : can.c
 * date   : 2016-07-08
 */

#include "can.h"

uint16_t can_generate_standard_id(uint16_t type, uint16_t src, uint16_t dest) {
    return (type << 8) | ((src & 0x0F) << 4) | (dest & 0x0F);
}

uint8_t can_get_message_type_from_id(uint16_t standard_id) {
    return standard_id >> 8;
}

uint8_t can_get_src_from_id(uint16_t standard_id) {
    return (standard_id & 0x00F0) >> 4;
}

uint8_t can_get_dest_from_id(uint16_t standard_id) {
    return standard_id & 0x000F;
}
