# Sample1

シリアル、CANの標準的なインターフェースを実装しておいたもの

## 機能

- カメラ通信 : camera-stream
- 音声通信 : sound
- TPIPの各種IO : control
- シリアル通信 : serial
    - クライアントから送ったデータをTPIPのシリアルポートに流す
    - TPIPがしりあるから受け取ったデータを文字列でクライアントに送る
- CAN通信 : can
    - TPIPのCANポートからデータを送る
    - フォーマットは以下で固定
        ~~~
            {
                "dest": 0 ~ 15,
                "command": uint8_t,
                "arg1": int16_t,
                "arg2": int16_t
            }
        ~~~
    - 以下の形で送られる        
        - standerd_id : type(3bit) | 送信元ID(4bit) | dest(4bit)        
        - 0byte : command        
        - 1,2byte : arg2        
        - 3,4byte : arg3
    - データは内部で(dest, command) をキーにバッファリングしており、同じキーで未送信の古いデータは捨てられる
    - 受信は未実装な気がする
- シリアル中継CAN通信 :serialcan
    - can通信のデータを、適当にシリアライズしてシリアルポートに流す
    - インターフェースはcanと完全に同じ
    - 第１６回で使用したCanMotorBoard のサンプルに入っているであろうSerialRelayedCanプログラムを使うことでデータを復元してCANに流せる