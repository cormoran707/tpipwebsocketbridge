/**
 * 
 * 実装が必要な関数
 * int robot_setup()
 * int robot_loop()
 * int robot_on_receive(const json_t *rcv_json)
 * int robot_on_send(json_t *send_json)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <array>
#include <thread>

#include "robot.h"          // include必須(robot_...が宣言されている)
#include "jansson.h"        // jsonパーサーライブラリ
#include "tpip.h"           // tpipライブラリのラッパー関数群
#include "tpip_json.h"      // tpipのデータをjson_tで取得・設定できる
#include "config_manager.h" // config.jsonから設定を読み込み・取得する

#include "serial_relayed_can.h"
#include "can.h"

#include "system.hpp"

SendBuffer<(uint8_t)CAN_ID::TPIP> can_send_buffer;

/**
 *
 * TPIP API 初期化直後に呼ばれる関数
 * 初期位置等の設定をすると良い
 * return : -1でエラー（現状何もしない）
 *           0で正常
 *
 */
int robot_setup() {
	tpip_connect_serial(json_string_value(config_get("ip")) , 9000);
    return 0;
}


/**
 *
 * 定期的に呼ばれる関数
 * ただし呼ばれる間隔は他の処理の時間によって変動する
 * return : -1でエラー（現状何もしない）
 *           0で正常
 *
 */
int robot_loop() {

	static auto prev = std::chrono::steady_clock::now();
	auto now = std::chrono::steady_clock::now();
	if (now - prev > std::chrono::milliseconds(90)) {
		can_send_buffer.send();
		prev = now;
	}
    return 0;
}


/**
 *
 * 制御情報を受信した時に呼ばれる関数
 *
 * rcv_json : 受信したjsonデータ 向こうで解放されるので勝手にdecrefしないで。
 *
 * return : -1でエラー（接続を打ち切る）
 *           0で正常
 *
 */
int robot_on_receive(const json_t *rcv_json) {
    tpip_set_control_by_json(rcv_json);
    
    const json_t *root = rcv_json;    


    // シリアル通信でCANデータを送る
    // buffer によって、いい感じの速度で送ってくれる。
    // bufferは、同一のdest, 同一のcommand のデータでまだ送っていないものがある場合、古い方を消して最新のものだけが保持されるようになってる
    json_t *serial_can = json_object_get(root, "serial_can");
    if(json_is_object(serial_can)) {
        json_t *dest_j = json_object_get(serial_can, "dest");
        json_t *command_j = json_object_get(serial_can, "command"); // 8bit
        json_t *arg1_j = json_object_get(serial_can, "arg1"); // 16bit
        json_t *arg2_j = json_object_get(serial_can, "arg1"); // 16bit

        if(json_is_number(dest_j) && json_is_number(command_j) && json_is_number(arg1_j) && json_is_number(arg2_j)) {
            uint8_t dest = json_number_value(dest_j);
            uint8_t command = json_number_value(command_j);
            int16_t arg1 = json_number_value(arg1_j);
            int16_t arg2 = json_number_value(arg2_j);

            can_send_buffer.update(dest, command, arg1, arg2); // 1 + 2 + 2 = 5byte
        }
    }

    json_t *serial = json_object_get(root, "serial");
    if(json_is_string(serial)) {
        const char* str = json_string_value(serial);
        if(str != NULL) {
            tpip_send_serial(str, sizeof(str));
        }
    }


    json_t *can = json_object_get(root, "can");
    if(json_is_object(can)) {
        json_t *dest_j = json_object_get(serial_can, "dest");
        json_t *command_j = json_object_get(serial_can, "command"); // 8bit
        json_t *arg1_j = json_object_get(serial_can, "arg1"); // 16bit
        json_t *arg2_j = json_object_get(serial_can, "arg1"); // 16bit

        if(json_is_number(dest_j) && json_is_number(command_j) && json_is_number(arg1_j) && json_is_number(arg2_j)) {
            uint8_t dest = json_number_value(dest_j);
            uint8_t command = json_number_value(command_j);
            int16_t arg1 = json_number_value(arg1_j);
            int16_t arg2 = json_number_value(arg2_j);

            struct tpip_can_data data;
            data.standard_id =
                    can_generate_standard_id(CAN_TYPE_COMMAND, (uint8_t)CAN_ID::TPIP, dest);
            data.size = send_can_data_rec(data.data, 0, command, arg1, arg2);

            tpip_send_can(&data);
        }
    }
    return 0;
}

/**
 *
 * センサ情報を情報を送信する前に呼ばれる関数
 *
 * send_json : 送信するjsonデータ 制御ボードのセンサ情報が格納されている状態
 *             !! send_json に追加する値は全て所有権を譲渡しないといけない(_newな関数でaddする)
 * return : -1でエラー（送信しない）
 *           0で正常
 *
 */
int robot_on_send(json_t *send_json) {
    {
        json_t *tpip_sensor_json = tpip_get_sensor_as_json();
        json_object_update(send_json, tpip_sensor_json);
        json_decref(tpip_sensor_json);
    }

    return 0;
}


int main() {
    return server_main();
}
