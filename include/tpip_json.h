#ifndef TPIP_JSON_H
#define TPIP_JSON_H

#include "tpip.h"
#include "jansson.h"

#ifdef __cplusplus
extern "C" {
#endif

extern json_t *tpip_get_sensor_as_json(); // must call decref after use.
extern json_t *tpip_get_control_as_json(); // must call decref after use.
extern bool tpip_set_control_by_json(const json_t* json_data);

extern json_t* tpip_convert_sensor_to_json(const struct tpip_sensor_data *sens);
// JSONで指定されていない要素はそのままで変更しない
extern bool tpip_convert_json_to_control(const json_t *json, struct tpip_control_data *ctl);

extern json_t *tpip_convert_control_to_json(const struct tpip_control_data *ctl);

#ifdef __cplusplus
}
#endif
    
#endif
