#ifndef MYTIME_H
#define MYTIME_H

typedef unsigned long mytime_t;

extern mytime_t mytime_get_time();

#endif
