#ifndef INTERVAL_TIMER_H
#define INTERVAL_TIMER_H

#include<time.h>
#include"mytime.h"

#ifdef __cplusplus
extern "C" {
#endif

struct interval_timer_t {
    mytime_t prev;
    double duration_s;
    void (*f)(void);
    struct interval_timer_t *next;
};

extern struct interval_timer_t* interval_timer_new();
extern void interval_timer_set_interval(struct interval_timer_t *timer,
                                        void (*f)(void), unsigned long duration_ms);
extern void interval_timer_tick(struct interval_timer_t *timer);
extern void interval_timer_destroy(struct interval_timer_t *timer);

#ifdef __cplusplus
}
#endif

#endif
