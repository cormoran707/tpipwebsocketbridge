# TpipWebSocketBridge (ライブラリ)

Tpip2, 3の制御・映像・音声通信のwebsocketインターフェース

TPIPとの接続はWindowsのみ

Mac, Linuxでもエミュレートモードで動かせる（開発用）

ライブラリ部はTPIPの内部でも動かせる（サンプルはc++使ってるので厳しい）

---

## ダウンロード

~~~
git clone --recursive https://bitbucket.org/cormoran707/tpipwebsocketbridge
#
# 普通にcloneしてしまったあなたは
#
git submodule init
git submodule update
~~~

---

## 依存ライブラリ

- libwebsockets(git submoduleで勝手に取ってくる)  : LGPL
- libtpip ( 〃 ) : 内部でTpipWikiのライセンスのライブラリ
- jansson ( 〃 ) : MIT
- OpenCV3（TARGET=TPIP_EMULATOR かつ TPIP_EMULATOR_USE_OPENCV=ON でコンパイル時のみ必要）

---

## [コンパイル](readme.compile.md)

## [サーバーAPIリファレンス](readme.api.md)

## [クライアントサンプル](client/readme.md)

## [各ロボット用にカスタマイズ](readme.customize.md)

---

## 設定ファイルの利用

バイナリと同じディレクトリにconfig.jsonを置き、設定を書くことでサーバーの設定を変更することが可能

ファイルがなかったら 192.168.0.200に繋いだり、適当にやってくれる

[設定ファイルのページ](readme.configjson.md)参照


---

## 動作確認したことのある環境

最新版で動く保証はない

TPIPと通信できるのはWindowsのみで、unixでは適当にエミュレートするだけなので注意

- Windows10 VisualStudio 2015
- MacOSX EI Capitan clang (LLVM7.0)
- Ubuntu 14 (gcc4.8)